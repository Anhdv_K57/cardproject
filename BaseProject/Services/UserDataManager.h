//
//  UserManager.h
//  BaseProject
//
//  Created by Chung BD on 6/3/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataManager : NSObject
@property (atomic,strong) NSString* id;
@property (atomic,strong) NSString* email;
@property (atomic,strong) NSString* name;
@property (atomic,strong) NSString* phone;
@property (atomic,strong) NSString* group;
@property (atomic,strong) NSString* balance_formatted;
@property (atomic,strong) NSString* created_formatted;
@property (atomic,strong) NSString* token;

- (void) getUser:(NSString*)id email:(NSString*)email name:(NSString*)name phone:(NSString*)phone group:(NSString*)group balance_formatted:(NSString*)balance_formatted created_formatted:(NSString*)created_formatted token:(NSString*)token;
@end
