//
//  UserManager.m
//  BaseProject
//
//  Created by Chung BD on 6/3/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import "UserDataManager.h"

@implementation UserDataManager

- (void)getUser:(NSString *)id email:(NSString *)email name:(NSString *)name phone:(NSString *)phone group:(NSString *)group balance_formatted:(NSString *)balance_formatted created_formatted:(NSString *)created_formatted token:(NSString *)token{
    _id=id;
    _name=name;
    _email=email;
    _phone=phone;
    _group=group;
    _balance_formatted=balance_formatted;
    _created_formatted=created_formatted;
    _token=token;
}
@end
