//
//  Constants.h
//  BaseProject
//
//  Created by Chung BD on 5/31/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#ifndef BaseProject_Constants_h
#define BaseProject_Constants_h

#define api_key @"d7fc60f2d45f538b849b64bde310e79b"

#define baseUrl @"http://trongthe.net/api_app/"

#define loginUrl [NSString stringWithFormat:@"%@acc_login.html",baseUrl]
#define loginParams @[@"email",@"password"]

#define registerUrl [NSString stringWithFormat:@"%@acc_register.html",baseUrl]
#define registerParams @[@"email",@"password",@"name",@"phone"]

#pragma - mark Game

#define productGameUrl [NSString stringWithFormat:@"%@product_category_code_game.html",baseUrl]
#define productGameParams nil

#pragma - mark Phone

#define productPhoneUrl [NSString stringWithFormat:@"%@product_category_code_mobile.html",baseUrl]
#define productPhoneParams nil

#define chargePhoneUrl [NSString stringWithFormat:@"%@order_topup_mobile.html",baseUrl]
#define chargePhoneParams @[@"type_child",@"type_child_value",@"mobile",@"uid",@"token"]

#pragma - mark Other

#define productOtherUrl [NSString stringWithFormat:@"%@product_category_code_other.html",baseUrl]
#define productOtherParams nil

#pragma - mark Game+Phone+Other

#define productListUrl [NSString stringWithFormat:@"%@product_category_code_other.html",baseUrl]
#define productListParams @[@"category",@"type",@"type_child"]

#define purchaseCardUrl [NSString stringWithFormat:@"%@order_card_code.html",baseUrl]
#define purchaseCardParams @[@"type_child",@"type_child_value",@"quantity",@"uid",@"token"]
#endif
