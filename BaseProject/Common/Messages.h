//
//  Messages.h
//  BaseProject
//
//  Created by Chung BD on 6/4/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#ifndef BaseProject_Messages_h
#define BaseProject_Messages_h

#define MSG_WRONG_EMAIL_AT_LOGIN @"Email không hợp lệ, xin thử lại."

#define MSG_NEED_ENTER_EMAIL @"Xin điền email hoặc số điện thoại đã đăng ký!"

#define MSG_NEED_ENTER_PASSWORD @"Xin điền password!"

#define MSG_NEED_CHECK_PASSWORD_AND_CONFIRM_PW @"Xin kiểm tra và xác nhận password!"

#define MSG_NEED_TO_FILL_ALL_FIELD @"Xin điền đầy đủ thông tin!"

#define MSG_CHECK_INTERNET_CONNECTION @"Không có kết nối. Xin kiểm tra lại \r\n kết nối internet."

#define MSG_UNKNOWN_ERROR @"Có lỗi xảy ra!"
#endif
