//
//  Utils.h
//  BaseProject
//
//  Created by Chung BD on 5/31/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject

+ (void)goToHome:(UIViewController*)viewController withAction:(void (^)(void))action;
+ (void)padTextField:(UITextField*)textField;
+ (void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message;
@end
