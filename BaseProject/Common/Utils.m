//
//  Utils.m
//  BaseProject
//
//  Created by Chung BD on 5/31/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (void)goToHome:(UIViewController*)viewController withAction:(void (^)(void))action{
    [viewController dismissViewControllerAnimated:YES completion:action];
}
+ (void)padTextField:(UITextField *)textField{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 10)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
+ (void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}
@end
