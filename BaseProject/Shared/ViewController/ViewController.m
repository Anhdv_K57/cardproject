//
//  ViewController.m
//  BaseProject
//
//  Created by Chung BD on 5/31/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSUInteger selectedItemIndex;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Event Handlers
- (IBAction)goToMasterTab:(UIButton *)sender {
    @try {
        selectedItemIndex=sender.tag;
        [self performSegueWithIdentifier:@"toMasterTab" sender:self];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

- (IBAction)goToLoginRegister:(UIButton *)sender {
    @try {
        UIStoryboard* login=[UIStoryboard storyboardWithName:@"LoginRegister_iPhone" bundle:nil];
        LoginRegisterController* destination=[login instantiateViewControllerWithIdentifier:@"master"];
        destination.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
        destination.modalPresentationStyle=UIModalPresentationOverCurrentContext;
        destination.selectedTabIndex=sender.tag;
        [self presentViewController:destination animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toMasterTab"]){
        MasterTabController* destination=[segue destinationViewController];
        destination.selectedTabIndex=selectedItemIndex;
    }
}
@end
