//
//  RegisterController.m
//  BaseProject
//
//  Created by Do Viet Anh on 8/22/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import "RegisterController.h"

@interface RegisterController (){
    LibraryAPI* lib;
}

@end

@implementation RegisterController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self formatTextFields];
    lib=[LibraryAPI shareInstance];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma  mark - OnLoad Operation
- (void)formatTextFields{
    [Utils padTextField:_inemail];
    [Utils padTextField:_inpassword];
    [Utils padTextField:_inname];
    [Utils padTextField:_inphone];
    _inemail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:1.0 alpha:0.5]}];
    _inpassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mật khẩu" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:1.0 alpha:0.5]}];
    _inname.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Tên" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:1.0 alpha:0.5]}];
    _inphone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Số điện thoại" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:1.0 alpha:0.5]}];
}
#pragma mark - Event Handlers
- (IBAction)signup:(UIButton *)sender {
    @try {
        if(!_inemail.text.length||!_inpassword.text.length||!_inname.text.length||!_inphone.text.length)
            [Utils showAlertWithTitle:@"Lỗi" andMessage:MSG_NEED_TO_FILL_ALL_FIELD];
        else{
            NSDictionary* data=[[NSDictionary alloc] initWithObjects:@[_inemail.text,_inpassword.text,_inname.text,_inphone.text] forKeys:registerParams];
            [lib.network requestToServicesWithURL:registerUrl parameter:data completion:^(ErrorCodes code, id result, id message) {
                if(!result)
                    [Utils showAlertWithTitle:@"Lỗi" andMessage:[NSString stringWithFormat:@"%@",message]];
                else{
                    //người ta viết sai cái key formatted thành formated
                    [lib.user getUser:result[@"id"] email:result[@"email"] name:result[@"name"] phone:result[@"phone"] group:result[@"group"] balance_formatted:result[@"balance_formated"] created_formatted:result[@"created_formated"] token:result[@"token"]];
                    [Utils showAlertWithTitle:@"Thông báo" andMessage:@"Đăng ký thành công!"];
                    //hiển thị thông tin người dùng ở trang chủ, giấu nút đăng ký và đăng nhập, hiển thị nút đăng xuất
                    [Utils goToHome:self withAction:nil];
                }
            }];
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}
@end
