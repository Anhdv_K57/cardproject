//
//  LoginController.h
//  BaseProject
//
//  Created by Do Viet Anh on 8/22/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"

@interface LoginController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *inemail;
@property (strong, nonatomic) IBOutlet UITextField *inpassword;
- (IBAction)login:(UIButton *)sender;

@end
