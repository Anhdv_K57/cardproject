//
//  MasterTabController.h
//  BaseProject
//
//  Created by Do Viet Anh on 8/21/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"
#import "CommonVC.h"

@interface MasterTabController : UITabBarController
@property NSUInteger selectedTabIndex;
@end
