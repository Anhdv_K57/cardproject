//
//  OtherController.h
//  BaseProject
//
//  Created by Do Viet Anh on 8/21/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"

@interface OtherController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSMutableArray * arraySelectCar;
    NSMutableArray * arrayPriceCar;


}



@property (strong, nonatomic) IBOutlet UIPickerView *PickerSelectCar;
- (IBAction)btnSelectCar:(id)sender;
- (IBAction)btnPriceCar:(id)sender;
- (IBAction)goToHome:(UIButton *)sender;
@end
