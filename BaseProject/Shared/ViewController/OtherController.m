//
//  OtherController.m
//  BaseProject
//
//  Created by Do Viet Anh on 8/21/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import "OtherController.h"

@interface OtherController ()

@end

@implementation OtherController

- (void)viewDidLoad {
    [super viewDidLoad];
    arraySelectCar =[[NSMutableArray alloc]init];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return @"";
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - OnLoad Operation

#pragma mark - Event Handlers
- (IBAction)btnSelectCar:(id)sender {
}

- (IBAction)goToHome:(UIButton *)sender {
    @try {
        [Utils goToHome:self withAction:nil];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

- (IBAction)btnPriceCar:(id)sender {
}
@end
