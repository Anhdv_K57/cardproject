//
//  PhoneController.m
//  BaseProject
//
//  Created by Do Viet Anh on 8/21/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import "PhoneController.h"

@interface PhoneController (){
    NSMutableArray* subViewControllers;
}


@end

@implementation PhoneController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareTopTabbar];
    [self loadSubViewControllers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - OnLoad Operation
- (void)prepareTopTabbar{
    for(int i=0;i<_toptabbar.items.count;i++)
        [_toptabbar.items[i] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:13.0f], NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    _toptabbar.selectedItem=[_toptabbar.items objectAtIndex:_selectedTabIndex];
}
- (void)loadSubViewControllers{
    subViewControllers=[NSMutableArray arrayWithArray:@[[self.storyboard instantiateViewControllerWithIdentifier:@"nap"],[self.storyboard instantiateViewControllerWithIdentifier:@"mua"]]];
    for(int i=0;i<subViewControllers.count;i++){
        [self addChildViewController:subViewControllers[i]];
    }
    [self swapContainerToViewController:_selectedTabIndex];
}
#pragma mark - Event Handlers
- (IBAction)goToHome:(UIButton *)sender {
    @try {
        [Utils goToHome:self withAction:nil];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
#pragma mark - UITabBarDelegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    NSInteger justSelectedTabIndex=[[tabBar items] indexOfObject:item];
    if(justSelectedTabIndex!=_selectedTabIndex){
        [self swapContainerToViewController:justSelectedTabIndex];
        _selectedTabIndex=justSelectedTabIndex;
    }
}
#pragma mark - Container
- (void)swapContainerToViewController:(NSInteger)index{
    if(self.childViewControllers.count>0){
        [self transitionFromViewController:subViewControllers[_selectedTabIndex] toViewController:self.childViewControllers[index] duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
            [self.container addSubview:((UIViewController*)self.childViewControllers[index]).view];
            [subViewControllers[index] didMoveToParentViewController:self];
        }];
    }
}
@end
