//
//  RegisterController.h
//  BaseProject
//
//  Created by Do Viet Anh on 8/22/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"

@interface RegisterController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *inemail;
@property (strong, nonatomic) IBOutlet UITextField *inpassword;
@property (strong, nonatomic) IBOutlet UITextField *inname;
@property (strong, nonatomic) IBOutlet UITextField *inphone;
- (IBAction)signup:(UIButton *)sender;

@end
