//
//  CommonVC.h
//  BaseProject
//
//  Created by Chung BD on 5/31/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#ifndef BaseProject_CommonVC_h
#define BaseProject_CommonVC_h
#import "PhoneController.h"
#import "GameController.h"
#import "OtherController.h"
#import "NewsController.h"
#import "ContactController.h"
#import "LoginRegisterController.h"
#endif
