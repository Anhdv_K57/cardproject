//
//  LoginRegisterController.h
//  BaseProject
//
//  Created by Do Viet Anh on 8/22/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"
#import "LoginController.h"

@interface LoginRegisterController : UIViewController<UITabBarDelegate>
@property NSInteger selectedTabIndex;
@property (strong, nonatomic) IBOutlet UITabBar *toptabbar;
@property (strong, nonatomic) IBOutlet UIView *container;

- (IBAction)goToHome:(UIButton *)sender;

@end
