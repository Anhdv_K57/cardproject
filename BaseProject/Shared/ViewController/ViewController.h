//
//  ViewController.h
//  BaseProject
//
//  Created by Chung BD on 5/31/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterTabController.h"

@interface ViewController : UIViewController

- (IBAction)goToMasterTab:(UIButton *)sender;
- (IBAction)goToLoginRegister:(UIButton *)sender;

@end

