//
//  MasterTabController.m
//  BaseProject
//
//  Created by Do Viet Anh on 8/21/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import "MasterTabController.h"

@interface MasterTabController ()

@end

@implementation MasterTabController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTabbarItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - OnLoad Operation
- (void)addTabbarItems{
    UIStoryboard* phone=[UIStoryboard storyboardWithName:@"PhoneItems_iPhone" bundle:nil];
    UIStoryboard* game=[UIStoryboard storyboardWithName:@"GameItems_iPhone" bundle:nil];
    UIStoryboard* other=[UIStoryboard storyboardWithName:@"OtherItems_iPhone" bundle:nil];
    UIStoryboard* news=[UIStoryboard storyboardWithName:@"NewsItems_iPhone" bundle:nil];
    UIStoryboard* contact=[UIStoryboard storyboardWithName:@"ContactItems_iPhone" bundle:nil];
    [self setViewControllers:@[[phone instantiateViewControllerWithIdentifier:@"master"],
                               [game instantiateViewControllerWithIdentifier:@"master"],
                               [other instantiateViewControllerWithIdentifier:@"master"],
                               [news instantiateViewControllerWithIdentifier:@"master"],
                               [contact instantiateViewControllerWithIdentifier:@"master"]]];
    [self setSelectedIndex:_selectedTabIndex];
}
@end
